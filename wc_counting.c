///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 02 - WordCount
///
/// @file wc_counting.c
/// @version 2.0
///
/// @author Robert Lemon <rlemon@hawaii.edu>
/// @brief  Lab 02 - WordCount - EE 491F - Spr 2021
/// @date 26_jan_2021
///////////////////////////////////////////////////////////////////////////////

// Header file
#include "wc_counting.h"

// Global variables
int charCount = 0;
int wordCount = 0;
int lineCount = 0;

int charCountTotal = 0;
int wordCountTotal = 0;
int lineCountTotal = 0;

/**
 * Function that determines if a character is whitespace.
 *   INPUT: char
 *   OUTPUT: bool (true if whitespace, false if not)
 */
bool isWhitespace(const char ch) {

   bool isWhitespace;

   // check if ch is whitespace
   switch(ch) {
      case ' ':
      case '\t':
      case '\n':
      case '\f':
      case '\r':
      case '\v':
         isWhitespace = true;
         break;
      default:
         isWhitespace = false;
   }

   return isWhitespace;
}

/**
 * Function that opens a given file and performs a wordcount
 * on the file, counting bytes, words, and lines, stored globally.
 *   INPUT: filename string
 *   OUTPUT: none
 */
void fileWordCount(const char *fileName){

   char ch;
   bool inWord = false;
   FILE * file;

   // Attempt to open the file, otherwise exit
   if ( (file = fopen(fileName, "r")) == NULL ) {
      fprintf(stderr, "%s: Can't open [%s]\n",
            program, fileName);
      return;
   }

   // Reset file counters
   charCount = 0;
   wordCount = 0;
   lineCount = 0;

   // Read file
   while ( (ch = fgetc(file)) != EOF ) {
      // count lines
      if (ch == '\n') {
         lineCount++;
         lineCountTotal++;
      }
      // count words
      if (!isWhitespace(ch)) {
         if (!inWord) {
            wordCount++;
            wordCountTotal++;
         }
         inWord = true;
      } else {
         inWord = false;
      }
      // count chars
      charCount++;
      charCountTotal++;
   }

   // Print the current file's count
   printCurrentCounts(fileName);

   // Attempt to close file, otherwise exit
   if (fclose(file) == EOF) {
      fprintf(stderr, "%s: Can't close [%s]\n", program, fileName);
      exit(EXIT_FAILURE);
   }

   return;
}

/**
 * Function that performs a word count on stdin, counting bytes, words, and lines.
 * The counts are stored globally.
 *   INPUT: none
 *   OUTPUT: none
 */
void stdinWordCount(){

   char ch;
   bool inWord = false;

   // Read stdin until EOF
   while ( (ch = getchar()) != EOF ) {
      // count lines
      if (ch == '\n') {
         lineCount++;
         lineCountTotal++;
      }
      // count words
      if (!isWhitespace(ch)) {
         if (!inWord) {
            wordCount++;
            wordCountTotal++;
         }
         inWord = true;
      } else {
         inWord = false;
      }
      // count chars
      charCount++;
      charCountTotal++;
   }

   // Print the stdio's count
   printf("\n");
   printCurrentCounts("\b");

   return;
}
