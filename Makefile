# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc

all: $(TARGET)

wc: driver.c wc_counting.c wc_printing.c
	$(CC) $(CFLAGS) -o $(TARGET) driver.c wc_counting.c wc_printing.c

clean:
	rm $(TARGET)

test:
	chmod +x wc-testing.sh
	./wc-testing.sh
