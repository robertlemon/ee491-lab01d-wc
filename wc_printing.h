///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 02 - WordCount
///
/// @file wc_counting.h
/// @version 2.0
///
/// @author Robert Lemon <rlemon@hawaii.edu>
/// @brief  Lab 02 - WordCount - EE 491F - Spr 2021
/// @date 26_jan_2021
///////////////////////////////////////////////////////////////////////////////

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// [driver] global variables 
extern char *program;

extern bool onlyLines;
extern bool onlyWords;
extern bool onlyChars;

// [wc_counting] global variables
extern int charCount;
extern int wordCount;
extern int lineCount;

extern int charCountTotal;
extern int wordCountTotal;
extern int lineCountTotal;

// Function prototypes
void printCurrentCounts(const char *fileName);
void printTotalCounts();
