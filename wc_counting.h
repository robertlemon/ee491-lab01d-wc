///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 02 - WordCount
///
/// @file wc_counting.h
/// @version 2.0
///
/// @author Robert Lemon <rlemon@hawaii.edu>
/// @brief  Lab 02 - WordCount - EE 491F - Spr 2021
/// @date 26_jan_2021
///////////////////////////////////////////////////////////////////////////////

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Global variables 
extern char *program;

// Function prototypes
bool isWhitespace(const char ch);
void fileWordCount(const char *fileName);
void stdinWordCount();

void printCurrentCounts(const char *fileName);
