make
echo Creating [test5]
touch test5
chmod -rwx test5
echo
echo ======================== TESTING ========================
echo
echo The counts for [test1] through [test4] are hardcoded
echo from the real wc program results. Any errors in these
echo tests will appear below:
echo
# Test1
if ./wc test1 | grep -q -v 34; then
   echo ERROR! Incorrect line count on test1.
fi
if ./wc test1 | grep -q -v 463; then
   echo ERROR! Incorrect word count on test1.
fi
if ./wc test1 | grep -q -v 2212; then
   echo ERROR! Incorrect char count on test1.
fi

# Test2
if ./wc test2 | grep -q -v 5; then
   echo ERROR! Incorrect line count on test2.
fi
if ./wc test2 | grep -q -v 3; then
   echo ERROR! Incorrect word count on test2.
fi
if ./wc test2 | grep -q -v 24; then
   echo ERROR! Incorrect char count on test2.
fi

# Test3
if ./wc test3 | grep -q -v 7; then
   echo ERROR! Incorrect line count on test3.
fi
if ./wc test3 | grep -q -v 40; then
   echo ERROR! Incorrect word count on test3.
fi
if ./wc test3 | grep -q -v 157; then
   echo ERROR! Incorrect char count on test3.
fi

# Test4
if ./wc test4 | grep -q -v 7; then
   echo ERROR! Incorrect line count on test4.
fi
if ./wc test4 | grep -q -v 40; then
   echo ERROR! Incorrect word count on test4.
fi
if ./wc test4 | grep -q -v 302; then
   echo ERROR! Incorrect char count on test4.
fi

# Test5
echo ---------------------------------------------------------
echo For [test5], an error message should appear below
echo stating that [test5] can\'t be opened:
echo
./wc test5
echo

# Argument Testing
echo ---------------------------------------------------------
echo For the argument testing, any errors will appear
echo above the version information:
echo
if ./wc -l test1 | grep -q -v 34; then
   echo ERROR! Incorrect line count on test1.
fi
if ./wc -w test1 | grep -q -v 463; then
   echo ERROR! Incorrect word count on test1.
fi
if ./wc -c test1 | grep -q -v 2212; then
   echo ERROR! Incorrect char count on test1.
fi
echo
./wc --version
echo
echo ---------------------------------------------------------
echo
echo Testing finished.
echo
echo =========================================================
