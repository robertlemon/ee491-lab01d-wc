///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 02 - WordCount
///
/// @file driver.c
/// @version 2.0
///
/// @author Robert Lemon <rlemon@hawaii.edu>
/// @brief  Lab 02 - WordCount - EE 491F - Spr 2021
/// @date 26_jan_2021
/// @todo Implement UTF-16 support
///////////////////////////////////////////////////////////////////////////////

// Header file
#include "driver.h"

// Global variables 
char *program;

bool onlyLines = false;
bool onlyWords = false;
bool onlyChars = false;

/**
 * Main function that runs when program executes.
 * Stores program name globally, checks argument quality,
 * then calls wordCount function.
 */
int main(int argc, char *argv[]) {

   int i;
   int fileCount = 0;
   int fileArgs[argc-1];

   // Check for improper amount of arguments
   if (argc < 1) {
      fprintf(stderr, "%s: Usage:  wc FILE\n", program);
      exit(EXIT_FAILURE);
   }

   // Initialize fileArgs array
   for (i = 0; i < argc-1; i++) {
      fileArgs[i] = EMPTY;
   }

   // Update global program name
   program = argv[0] + 2;

   // Iterate over args from program call
   for (i = 1; i < argc; i++) {
      // Only count chars
      if ( (strcmp(argv[i], "-c") == 0) || 
            (strcmp(argv[i], "--bytes") == 0) ) {
         onlyChars = true;
      }
      // Only count words
      else if ( (strcmp(argv[i], "-w") == 0) || 
            (strcmp(argv[i], "--words") == 0) ) {
         onlyWords = true;
      }
      // Only count lines
      else if ( (strcmp(argv[i], "-l") == 0) || 
            (strcmp(argv[i], "--lines") == 0) ) {
         onlyLines = true;
      }
      // Print version information and exit
      else if ((strcmp(argv[i], "--version") == 0)) {
         printf("%s 2.0\n", program);
         printf("No copyright or license.\n");
         printf("This is free software: you are free to change and ");
         printf("redistribute it.\n");
         printf("There is NO WARRANTY, to the extent permitted by law.\n");
         printf("\nWritten by Robert Lemon.\n");
         exit(EXIT_SUCCESS);
      }
      // Add arg to flie list
      else {
         fileArgs[fileCount++] = i;
      }
   }

   // Iterate over args from fileArgs
   for (i = 0; i < argc-1; i++) {
      // Check that arg isn't empty
      if (fileArgs[i] == EMPTY) {
         break;
      }
      // Pass arg to fileWordCount
      fileWordCount(argv[fileArgs[i]]);
   }

   // If multiple files counted, print total
   if (fileCount > 1) {
      printTotalCounts();
   }

   // If no file specified, count stdin
   if (fileCount == 0) {
     stdinWordCount(); 
   }
   
   return EXIT_SUCCESS;
}
