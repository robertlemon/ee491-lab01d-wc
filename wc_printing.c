///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 02 - WordCount
///
/// @file wc_printing.h
/// @version 2.0
///
/// @author Robert Lemon <rlemon@hawaii.edu>
/// @brief  Lab 02 - WordCount - EE 491F - Spr 2021
/// @date 26_jan_2021
///////////////////////////////////////////////////////////////////////////////

// Header file
#include "wc_printing.h"

/**
 * A function that prints the current counts to the screen.
 * Counts are stored globally.
 *   INPUT: const string for filename
 *   OUTPUT: none
 */
void printCurrentCounts(const char *fileName) {

   // Print line count if requested
   if (onlyLines) {
      printf("%d", lineCount);
   }
   // Print word count if requested
   if (onlyWords) {
      if (onlyLines) {
         printf("\t");
      }
      printf("%d", wordCount);
   }
   // Print char count if requested
   if (onlyChars) {
      if (onlyLines || onlyWords) {
         printf("\t");
      }
      printf("%d", charCount);
   }

   // Print the full count if unspecified
   if (!onlyLines && !onlyWords && !onlyChars) {
      printf("%d\t%d\t%d\t%s\n", lineCount, wordCount, charCount, fileName);
   }
   // Otherwise finish the requested partial print
   else {
      printf("\t%s\n", fileName);
   }
}

/**
 * A function that prints the total counts to the screen.
 * Counts are stored globally.
 *   INPUT: none
 *   OUTPUT: none
 */
void printTotalCounts() {

   // Print line count if requested
   if (onlyLines) {
      printf("%d", lineCountTotal);
   }
   // Print word count if requested
   if (onlyWords) {
      if (onlyLines) {
         printf("\t");
      }
      printf("%d", wordCountTotal);
   }
   // Print char count if requested
   if (onlyChars) {
      if (onlyLines || onlyWords) {
         printf("\t");
      }
      printf("%d", charCountTotal);
   }

   // Print the full count if unspecified
   if (!onlyLines && !onlyWords && !onlyChars) {
      printf("%d\t%d\t%d\ttotal\n", lineCountTotal, wordCountTotal, charCountTotal);
   }
   // Otherwise finish the requested partial print
   else {
      printf("\ttotal\n");
   }
}
